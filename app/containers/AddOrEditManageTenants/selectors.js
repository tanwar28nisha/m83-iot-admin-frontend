import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the addOrEditManageTenants state domain
 */

const selectAddOrEditManageTenantsDomain = state =>
  state.addOrEditManageTenants || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by AddOrEditManageTenants
 */

const makeSelectAddOrEditManageTenants = () =>
  createSelector(
    selectAddOrEditManageTenantsDomain,
    substate => substate,
  );

export default makeSelectAddOrEditManageTenants;
export { selectAddOrEditManageTenantsDomain };
