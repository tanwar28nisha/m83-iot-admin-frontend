/**
 *
 * Asynchronously loads the component for AddOrEditManageTenants
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
