/*
 * AddOrEditManageTenants Messages
 *
 * This contains all the text for the AddOrEditManageTenants container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.AddOrEditManageTenants';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the AddOrEditManageTenants container!',
  },
});
