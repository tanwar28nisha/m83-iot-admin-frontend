/**
 *
 * AddOrEditManageTenants
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectAddOrEditManageTenants from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export function AddOrEditManageTenants() {
  useInjectReducer({ key: 'addOrEditManageTenants', reducer });
  useInjectSaga({ key: 'addOrEditManageTenants', saga });

  return (
    <React.Fragment>
      <Helmet>
        <title>AddOrEditManageTenants</title>
        <meta
          name="description"
          content="Description of AddOrEditManageTenants"
        />
      </Helmet>
      
    <header className="content-header d-flex">
        <div className="flex-30">
             <h6>Manage Tenant</h6>
        </div>
        <div className="flex-70 text-right">
            
        </div>
    </header>
    <div className="content-body">
        <div className="form-content-wrapper">
            <div className="form-header">
                <h6>Tenant Details</h6>
            </div>
            <div className="form-body">
                <div className="d-flex">
                    <div className="flex-50 pd-r-7">
                        <div className="form-group">
                            <label className="form-group-label">Tenant Name :</label>
                            <input type="text" className="form-control" />
                        </div>
                    </div>
                    <div className="flex-50 pd-l-7">
                        <div className="form-group">
                            <label className="form-group-label">Company Name : </label>
                            <input type="text" className="form-control" />
                        </div>
                    </div>
                    <div className="flex-50 pd-r-7">
                        <div className="form-group">
                            <label className="form-group-label">Description :</label>
                            <textarea rows="1" type="text" className="form-control" />
                        </div>
                    </div>
                    <div className="flex-25 pd-r-7 pd-l-7">
                        <div className="form-group">
                            <label className="form-group-label">Tenant Type :</label>
                            <div class="radio-button">
                                <span class="radio-button-text">IFLEX</span>
                                <input type="radio" className="form-control" name="tenantCategory" value="MAKER" />
                                <span class="radio-button-mark"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="form-header">
                <h6>Add Admin</h6>
            </div>
            <div className="form-body">
                <div className="d-flex">
                    <div className="flex-50 pd-r-7">
                        <div className="form-group">
                            <label className="form-group-label">First Name :</label>
                            <input type="text" className="form-control" />
                        </div>
                    </div>
                    <div className="flex-50 pd-l-7">
                        <div className="form-group">
                            <label className="form-group-label">Last Name : </label>
                            <input type="text" className="form-control" />
                        </div>
                    </div>
                    <div className="flex-50 pd-r-7">
                        <div className="form-group">
                            <label className="form-group-label">Email : </label>
                            <input type="email" className="form-control" />
                        </div>
                    </div>
                    <div className="flex-50 pd-l-7">
                        <div className="form-group">
                            <label className="form-group-label">Phone Number : </label>
                            <input type="tel" className="form-control" />
                        </div>
                    </div>
                </div>
            </div>
            <div className="form-footer">
                <button className="btn btn-secondary">Cancel</button>
                <button className="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
    </React.Fragment>
  );
}

AddOrEditManageTenants.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  addOrEditManageTenants: makeSelectAddOrEditManageTenants(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(AddOrEditManageTenants);
