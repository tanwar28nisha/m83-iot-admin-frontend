import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the addOrEditManageLicense state domain
 */

const selectAddOrEditManageLicenseDomain = state =>
  state.addOrEditManageLicense || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by AddOrEditManageLicense
 */

const makeSelectAddOrEditManageLicense = () =>
  createSelector(
    selectAddOrEditManageLicenseDomain,
    substate => substate,
  );

export default makeSelectAddOrEditManageLicense;
export { selectAddOrEditManageLicenseDomain };
