/*
 * AddOrEditManageLicense Messages
 *
 * This contains all the text for the AddOrEditManageLicense container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.AddOrEditManageLicense';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the AddOrEditManageLicense container!',
  },
});
