/**
 *
 * Asynchronously loads the component for AddOrEditManageLicense
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
