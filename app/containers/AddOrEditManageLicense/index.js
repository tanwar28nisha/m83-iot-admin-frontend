/**
 *
 * AddOrEditManageLicense
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectAddOrEditManageLicense from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export function AddOrEditManageLicense(props) {
  useInjectReducer({ key: 'addOrEditManageLicense', reducer });
  useInjectSaga({ key: 'addOrEditManageLicense', saga });

  return (
    <React.Fragment>
      <Helmet>
        <title>AddOrEditManageLicense</title>
        <meta
          name="description"
          content="Description of AddOrEditManageLicense"
        />
      </Helmet>

    <header className="content-header d-flex">
        <div className="flex-30">
             <h6>Manage License</h6>

        </div>
        <div className="flex-70 text-right">
            
        </div>
    </header>
    <div className="content-body" id="addOrEditLicense">
        <div className="form-content-wrapper">
            <ul className="nav nav-pills">
                <li className="nav-item">
                    <a className="nav-link active" data-toggle="pill" href="#home">Tenant Detail</a>
                </li>
                <li class="nav-item">
                    <a className="nav-link" data-toggle="pill" href="#menu1">Package Attributes</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" data-toggle="pill" href="#menu2">Package Menu</a>
                </li>
            </ul>

            <div className="tab-content">
                <div className="tab-pane active" id="home">
                    <div className="form-header">
                        <h6>Tenant Details</h6>
                    </div>
                    <div className="form-body">
                        <div className="d-flex">
                            <div className="flex-50 pd-r-7">
                                <div className="form-group">
                                    <label className="form-group-label">Name :</label>
                                    <input type="text" className="form-control" />
                                </div>
                            </div>
                            <div className="flex-50 pd-l-7">
                                <div className="form-group">
                                    <label className="form-group-label">Amount(in USD) : </label>
                                    <input type="number" className="form-control" />
                                </div>
                            </div>
                            <div className="flex-50 pd-r-7">
                                <div className="form-group">
                                    <label className="form-group-label">Description :</label>
                                    <textarea rows="1" type="text" className="form-control" />
                                </div>
                            </div>
                            <div className="flex-50 pd-l-7">
                                <div className="form-group">
                                        <label className="form-group-label">Validity : </label>
                                    <input type="number" className="form-control" />
                                </div>
                            </div>
                            <div className="flex-100">
                                <div className="form-group">
                                    <label className="form-group-label">Tenant Type :</label>
                                        <div className="d-flex">
                                            <div className="flex-33 pd-r-7">
                                                <div className="radio-button">
                                                    <span className="radio-button-text">M-SaaS</span>
                                                    <input type="radio" className="form-control" name="tenantCategory" value="MAKER" />
                                                    <span className="radio-button-mark"></span>
                                                </div>
                                            </div>
                                            <div className="flex-33 pd-l-7 pd-r-7">
                                                <div className="radio-button">
                                                    <span className="radio-button-text">E-SaaS</span>
                                                    <input type="radio" className="form-control" name="tenantCategory" value="MAKER" />
                                                    <span className="radio-button-mark"></span>
                                                </div>
                                            </div>
                                            <div className="flex-33 pd-l-7">
                                                <div className="radio-button">
                                                    <span className="radio-button-text">Custom</span>
                                                    <input type="radio" className="form-control" name="tenantCategory" value="MAKER" />
                                                    <span className="radio-button-mark"></span>
                                                </div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="tab-pane" id="menu1">
                    <div className="form-header">
                        <h6>Package Attributes</h6>
                    </div>
                    <div className="form-body">
                        <ul className="package-list">
                            <li>
                                <div className="package-list-item">
                                        <label className="check-box">
                                            <span className="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                            <span className="check-mark"></span>
                                        </label>
                                        <div className="input-group">
                                            <button className="btn btn-light input-group-prepend"><i className="fas fa-chevron-left"></i></button>
                                            <input className="form-control" type="number"/>
                                            <button className="btn btn-light input-group-append"><i className="fas fa-chevron-right"></i></button>
                                        </div>
                                </div>
                            </li>

                            <li>
                                <div className="package-list-item">
                                        <label className="check-box">
                                            <span className="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                            <span className="check-mark"></span>
                                        </label>
                                        <div className="input-group">
                                            <button className="btn btn-light input-group-prepend"><i className="fas fa-chevron-left"></i></button>
                                            <input className="form-control" type="number"/>
                                            <button className="btn btn-light input-group-append"><i className="fas fa-chevron-right"></i></button>
                                        </div>
                                </div>
                            </li>
                             <li>
                                <div className="package-list-item">
                                        <label className="check-box">
                                            <span className="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                            <span className="check-mark"></span>
                                        </label>
                                        <div className="input-group">
                                            <button className="btn btn-light input-group-prepend"><i className="fas fa-chevron-left"></i></button>
                                            <input className="form-control" type="number"/>
                                            <button className="btn btn-light input-group-append"><i className="fas fa-chevron-right"></i></button>
                                        </div>
                                </div>
                            </li> <li>
                                <div className="package-list-item">
                                        <label className="check-box">
                                            <span className="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                            <span className="check-mark"></span>
                                        </label>
                                        <div className="input-group">
                                            <button className="btn btn-light input-group-prepend"><i className="fas fa-chevron-left"></i></button>
                                            <input className="form-control" type="number"/>
                                            <button className="btn btn-light input-group-append"><i className="fas fa-chevron-right"></i></button>
                                        </div>
                                </div>
                            </li> <li>
                                <div className="package-list-item">
                                        <label className="check-box">
                                            <span className="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                            <span className="check-mark"></span>
                                        </label>
                                        <div className="input-group">
                                            <button className="btn btn-light input-group-prepend"><i className="fas fa-chevron-left"></i></button>
                                            <input className="form-control" type="number"/>
                                            <button className="btn btn-light input-group-append"><i className="fas fa-chevron-right"></i></button>
                                        </div>
                                </div>
                            </li> <li>
                                <div className="package-list-item">
                                        <label className="check-box">
                                            <span className="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                            <span className="check-mark"></span>
                                        </label>
                                        <div className="input-group">
                                            <button className="btn btn-light input-group-prepend"><i className="fas fa-chevron-left"></i></button>
                                            <input className="form-control" type="number"/>
                                            <button className="btn btn-light input-group-append"><i className="fas fa-chevron-right"></i></button>
                                        </div>
                                </div>
                            </li> <li>
                                <div className="package-list-item">
                                        <label className="check-box">
                                            <span className="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                            <span className="check-mark"></span>
                                        </label>
                                        <div className="input-group">
                                            <button className="btn btn-light input-group-prepend"><i className="fas fa-chevron-left"></i></button>
                                            <input className="form-control" type="number"/>
                                            <button className="btn btn-light input-group-append"><i className="fas fa-chevron-right"></i></button>
                                        </div>
                                </div>
                            </li> <li>
                                <div className="package-list-item">
                                        <label className="check-box">
                                            <span className="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                            <span className="check-mark"></span>
                                        </label>
                                        <div className="input-group">
                                            <button className="btn btn-light input-group-prepend"><i className="fas fa-chevron-left"></i></button>
                                            <input className="form-control" type="number"/>
                                            <button className="btn btn-light input-group-append"><i className="fas fa-chevron-right"></i></button>
                                        </div>
                                </div>
                            </li> <li>
                                <div className="package-list-item">
                                        <label className="check-box">
                                            <span className="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                            <span className="check-mark"></span>
                                        </label>
                                        <div className="input-group">
                                            <button className="btn btn-light input-group-prepend"><i className="fas fa-chevron-left"></i></button>
                                            <input className="form-control" type="number"/>
                                            <button className="btn btn-light input-group-append"><i className="fas fa-chevron-right"></i></button>
                                        </div>
                                </div>
                            </li> <li>
                                <div className="package-list-item">
                                        <label className="check-box">
                                            <span className="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                            <span className="check-mark"></span>
                                        </label>
                                        <div className="input-group">
                                            <button className="btn btn-light input-group-prepend"><i className="fas fa-chevron-left"></i></button>
                                            <input className="form-control" type="number"/>
                                            <button className="btn btn-light input-group-append"><i className="fas fa-chevron-right"></i></button>
                                        </div>
                                </div>
                            </li>                        </ul>
                    </div>
                </div>
                <div className="tab-pane" id="menu2">
                    <div className="form-header">
                        <h6>Package Menu</h6>
                    </div>
                    <div className="form-body">
                        <ul className="d-flex custom-menu-list">
                            <li>
                                <div className="custom-menu-list-item">
                                    <button className="btn text-red"><i className="far fa-minus"></i></button>
                                    <h6>Device Type </h6>

                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item active">
                                    <button className="btn text-success"><i className="far fa-plus"></i></button>
                                    <h6>Device Type </h6>
                                    
                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item">
                                    <button className="btn text-success"><i className="far fa-plus"></i></button>
                                    <h6>FaaS</h6>
                                    <p>Code Engine</p>
                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item">
                                    <button className="btn text-red"><i className="far fa-minus"></i></button>
                                    <h6>Device Type </h6>

                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item active">
                                    <button className="btn text-success"><i className="far fa-plus"></i></button>
                                    <h6>Device Type </h6>
                                    
                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item">
                                    <button className="btn text-success"><i className="far fa-plus"></i></button>
                                    <h6>FaaS</h6>
                                    <p>Code Engine</p>
                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item">
                                    <button className="btn text-red"><i className="far fa-minus"></i></button>
                                    <h6>Device Type </h6>

                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item active">
                                    <button className="btn text-success"><i className="far fa-plus"></i></button>
                                    <h6>Device Type </h6>
                                    
                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item">
                                    <button className="btn text-success"><i className="far fa-plus"></i></button>
                                    <h6>FaaS</h6>
                                    <p>Code Engine</p>
                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item">
                                    <button className="btn text-red"><i className="far fa-minus"></i></button>
                                    <h6>Device Type </h6>

                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item active">
                                    <button className="btn text-success"><i className="far fa-plus"></i></button>
                                    <h6>Device Type </h6>
                                    
                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item">
                                    <button className="btn text-success"><i className="far fa-plus"></i></button>
                                    <h6>FaaS</h6>
                                    <p>Code Engine</p>
                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item">
                                    <button className="btn text-red"><i className="far fa-minus"></i></button>
                                    <h6>Device Type </h6>

                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item active">
                                    <button className="btn text-success"><i className="far fa-plus"></i></button>
                                    <h6>Device Type </h6>
                                    
                                </div>
                            </li>
                            <li>
                                <div className="custom-menu-list-item">
                                    <button className="btn text-success"><i className="far fa-plus"></i></button>
                                    <h6>FaaS</h6>
                                    <p>Code Engine</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="form-footer">
                <button className="btn btn-secondary">Cancel</button>
                <button className="btn btn-primary">Save</button>
            </div>
        </div>   
    </div>
    </React.Fragment>
  );
}

AddOrEditManageLicense.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  addOrEditManageLicense: makeSelectAddOrEditManageLicense(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(AddOrEditManageLicense);
