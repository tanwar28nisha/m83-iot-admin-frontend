/**
 *
 * Asynchronously loads the component for SystemProperties
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
