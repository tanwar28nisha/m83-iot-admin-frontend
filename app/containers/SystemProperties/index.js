/**
 *
 * SystemProperties
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectSystemProperties from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export function SystemProperties() {
  useInjectReducer({ key: 'systemProperties', reducer });
  useInjectSaga({ key: 'systemProperties', saga });

  return (
    <React.Fragment>
      <Helmet>
        <title>SystemProperties</title>
        <meta name="description" content="Description of SystemProperties" />
      </Helmet>
    
    <header className="content-header">
      <h6>System Properties</h6>
    </header>

    <div className="content-body" id="systemt-properties">
        <ul className="system-list">
            <li>
                <p>S3 <strong>(2)</strong></p>
            </li>
            <li className="active">
                <p>SageMaker <strong>(5)</strong></p>
            </li>
            <li>
                <p>S3 <strong>(2)</strong></p>
            </li>
            <li>
                <p>SageMaker <strong>(5)</strong></p>
            </li>
            <li>
                <p>S3 <strong>(2)</strong></p>
            </li>
            <li>
                <p>SageMaker <strong>(5)</strong></p>
            </li>
            <li>
                <p>S3 <strong>(2)</strong></p>
            </li>
            <li>
                <p>SageMaker <strong>(5)</strong></p>
            </li>
            <li>
                <p>S3 <strong>(2)</strong></p>
            </li>
            <li>
                <p>SageMaker <strong>(5)</strong></p>
            </li>
            <li>
                <p>S3 <strong>(2)</strong></p>
            </li>
        </ul>
        <div className="content-table">
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th>Display Name</th>
                        <th>Key</th>
                        <th>Value</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Access Key</td>
                        <td>s3.accessKey</td>
                        <td>AKIA3ECFSMLF4MGLZ2CW</td>
                        <td><button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button></td>
                    </tr>
                    <tr>
                        <td>Bucket</td>
                        <td>s3.accessKey</td>
                        <td>AKIA3ECFSMLF4MGLZ2CW</td>
                        <td><button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button></td>
                    </tr>
                    <tr>
                        <td>Region</td>
                        <td>s3.accessKey</td>
                        <td>AKIA3ECFSMLF4MGLZ2CW</td>
                        <td><button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button></td>
                    </tr>
                    <tr>
                        <td>Secret Key</td>
                        <td>s3.accessKey</td>
                        <td>AKIA3ECFSMLF4MGLZ2CW</td>
                        <td><button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button></td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
    </React.Fragment>
  );
}

SystemProperties.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  systemProperties: makeSelectSystemProperties(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(SystemProperties);
