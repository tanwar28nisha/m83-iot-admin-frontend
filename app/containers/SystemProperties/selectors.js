import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the systemProperties state domain
 */

const selectSystemPropertiesDomain = state =>
  state.systemProperties || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by SystemProperties
 */

const makeSelectSystemProperties = () =>
  createSelector(
    selectSystemPropertiesDomain,
    substate => substate,
  );

export default makeSelectSystemProperties;
export { selectSystemPropertiesDomain };
