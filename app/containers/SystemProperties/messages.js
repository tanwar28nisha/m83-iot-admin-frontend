/*
 * SystemProperties Messages
 *
 * This contains all the text for the SystemProperties container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.SystemProperties';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the SystemProperties container!',
  },
});
