/**
 *
 * SideBar
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectSideBar from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export class SideBar extends React.Component {
  render(){
  
    return (
    <React.Fragment>
      <Helmet>
        <title>SideBar</title>
        <meta name="description" content="Description of SideBar" />
      </Helmet>
      <div className="background-overlay"></div>
    <div className="arc-overlay"></div>
      <div className="top-bar">
          <div className="top-bar-logo">
            <img src="https://content.iot83.com/m83/misc/flexLogoWhite.png" />
          </div>
          <ul className="nav-menu-list">
                <li>
                    <a className="active" onClick = {() => this.props.history.push('/HomePage')}>
                        <span><i className="fad fa-home"></i></span>
                        <strong>Home</strong>
                    </a>
                </li>
                <li>
                    <a onClick = {() => this.props.history.push('/ManageTenants')}>
                        <span><i className="fad fa-building"></i></span>
                        <strong>Manage Tenants</strong>
                    </a>
                </li>
                <li>
                    <a onClick = {() => this.props.history.push('/ManageLicense')}>
                        <span><i className="fad fa-file-certificate"></i></span>
                        <strong>Manage License</strong>
                    </a>
                </li>
                <li>
                    <a onClick = {() => this.props.history.push('/AddOns')}>
                        <span><i className="fad fa-archive"></i></span>
                        <strong>Manage Add-Ons</strong>
                    </a>
                </li>
                <li>
                    <a onClick = {() => this.props.history.push('/SystemProperties')}>
                        <span><i className="fad fa-cogs"></i></span>
                        <strong>System Properties</strong>
                    </a>
                </li>
         </ul>
         <div className="dropdown">
            <button className="btn btn-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Shakher Chauhan 
                <div className="initial">
                      SC      
                </div>
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <div className="initial">
                      SC      
                </div>
                <h6>Shakher Chauhan</h6>
                <p>System Admin</p>
                <a className="dropdown-item" href="#">Change Password</a>
                <button className="btn btn-logout">Logout</button>
            </div>
            </div>
      </div>
    </React.Fragment>
  );
}
}

SideBar.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  sideBar: makeSelectSideBar(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(SideBar);
