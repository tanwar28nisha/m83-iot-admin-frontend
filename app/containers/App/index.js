/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import SideBar from '../SideBar/index';

import LoginPage from 'containers/LoginPage/Loadable';
import HomePage from 'containers/HomePage/Loadable';
import ManageTenants from 'containers/ManageTenants/Loadable';
import ManageLicense from 'containers/ManageLicense/Loadable';
import AddOns from 'containers/AddOns/Loadable';
import SystemProperties from 'containers/SystemProperties/Loadable';

import AddOrEditManageTenants from 'containers/AddOrEditManageTenants/Loadable';
import AddOrEditManageLicense from 'containers/AddOrEditManageLicense/Loadable';
import AddOrEditAddOns from 'containers/AddOrEditAddOns/Loadable';

import NotFoundPage from 'containers/NotFoundPage/Loadable';

import GlobalStyle from '../../global-styles';

export default function App(props) {
  console.log(props)
  return (
    <React.Fragment>
      <SideBar {...props}/>

      <Switch>
        {/* <Route exact path="/" component={LoginPage} /> */}
        <Route exact path="/HomePage" component={HomePage} {...props}/>
        <Route path="/ManageTenants" component={ManageTenants} {...props}/>
        <Route path="/ManageLicense" component={ManageLicense} {...props}/>
        <Route path="/AddOns" component={AddOns} {...props}/>
        <Route path="/SystemProperties" component={SystemProperties} {...props}/>


        <Route path="/AddOrEditManageTenants" component={AddOrEditManageTenants} {...props}/>
        <Route path="/AddOrEditManageLicense" component={AddOrEditManageLicense} {...props}/>
        <Route path="/AddOrEditAddOns" component={AddOrEditAddOns} {...props}/>

        {/* <Route component={NotFoundPage} /> */}
      </Switch>
      <GlobalStyle />
    </React.Fragment>
  );
}
