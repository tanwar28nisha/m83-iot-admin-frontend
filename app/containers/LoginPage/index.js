/**
 *
 * LoginPage
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectLoginPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export function LoginPage() {
  useInjectReducer({ key: 'loginPage', reducer });
  useInjectSaga({ key: 'loginPage', saga });

  return (
    <React.Fragment>
      <Helmet>
        <title>LoginPage</title>
        <meta name="description" content="Description of LoginPage" />
      </Helmet>

      <div className="login-wrapper">
        <div className="background-overlay"></div>
        <div className="arc-overlay"></div>

        <div className="logo">
            <img src="https://content.iot83.com/m83/misc/flexLogo.png" />
        </div>

        <div className="login-form-box">
          <h1>Welcome to Flex83</h1>
          <h6>Connect. Configure. Visualize. Operate. Extend</h6>

          <button className="btn btn-info"><i className="fab fa-microsoft"></i>Login with Microsoft</button>
          <h3>Or</h3>

          <div className="form-group">
              <span className="form-group-icon"><i className="far fa-envelope"></i></span>
              <input type="text" className="form-control" placeholder="Username" />
          </div>
          <div className="form-group">
              <span className="form-group-icon"><i className="far fa-lock"></i></span>
              <input type="text" className="form-control" placeholder="Password" />
          </div>
          <button className="btn btn-theme">Login</button>
          <a href="">Forget Password ?</a>
          <h3>Or</h3>
          <button className="btn btn-info"><i className="fas fa-user mr-2"></i>Don't have an account ? Sign Up Here !</button>

        </div>

      </div>


    </React.Fragment>
  );
}

LoginPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  loginPage: makeSelectLoginPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(LoginPage);
