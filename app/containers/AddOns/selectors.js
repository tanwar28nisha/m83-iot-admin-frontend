import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the addOns state domain
 */

const selectAddOnsDomain = state => state.addOns || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by AddOns
 */

const makeSelectAddOns = () =>
  createSelector(
    selectAddOnsDomain,
    substate => substate,
  );

export default makeSelectAddOns;
export { selectAddOnsDomain };
