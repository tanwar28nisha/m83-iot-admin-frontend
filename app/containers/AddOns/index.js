/**
 *
 * AddOns
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectAddOns from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export class AddOns extends React.Component {
    render() {

  return (
    <React.Fragment>
      <Helmet>
        <title>AddOns</title>
        <meta name="description" content="Description of AddOns" />
      </Helmet>

    <header className="content-header d-flex">
        <div className="flex-30">
             <h6>Add-Ons</h6>
        </div>
        <div className="flex-70 position-relative">
            <div className="content-header-group">
                <div className="search-group">
                    <i className="far fa-search"></i>
                    <input type="search" className="form-control" placeholder="Search....." />
                </div>
                <button onClick = {() => this.props.history.push('/AddOrEditAddOns')} className="btn btn-primary"><i className="far fa-plus"></i>Add New</button>
            </div>
        </div>
    </header>
    <div className="content-body">
        <div className="list-view-wrapper">
            <div className="list-view-header">
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <h1>Name</h1>
                    </li>
                    <li className="flex-10">
                        <h1>Category</h1>
                    </li>
                    <li  className="flex-10">
                        <h1>Published</h1>
                    </li>
                    <li  className="flex-10">
                        <h1>Type</h1>
                    </li>
                    <li  className="flex-10">
                        <h1>Amount</h1>
                    </li>
                    <li  className="flex-10">
                        <h1>Subscriptions</h1>
                    </li>
                    <li  className="flex-10">
                        <h1>Products</h1>
                    </li>
                    <li  className="flex-15">
                        <h1>Created At</h1>
                    </li>
                    <li  className="flex-10">
                        <h1>Actions</h1>
                    </li>
                </ul>
            </div>
            <div className="list-view-body">
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p>Codes Packag</p>
                        <h2>code catagory</h2>
                    </li>
                    <li className="flex-10">
                        <p>Code</p>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-green"><i class="fad fa-check-circle mr-2"></i>Yes</h3>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-primary"><i class="fad fa-globe mr-2"></i>Public</h3>
                    </li>
                    <li  className="flex-10">
                        <p><strong>10$</strong>USD</p>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-success">3</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">10</div>
                    </li>
                    <li  className="flex-15">
                    <p>1/22/2021, 5:30:00 AM</p>
                    </li>

                    <li  className="flex-10">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p>Codes Packag</p>
                        <h2>code catagory</h2>
                    </li>
                    <li className="flex-10">
                        <p>Code</p>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-green"><i class="fad fa-check-circle mr-2"></i>Yes</h3>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-primary"><i class="fad fa-globe mr-2"></i>Public</h3>
                    </li>
                    <li  className="flex-10">
                        <p><strong>10$</strong>USD</p>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-success">3</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">10</div>
                    </li>
                    <li  className="flex-15">
                    <p>1/22/2021, 5:30:00 AM</p>
                    </li>

                    <li  className="flex-10">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p>Codes Packag</p>
                        <h2>code catagory</h2>
                    </li>
                    <li className="flex-10">
                        <p>Code</p>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-green"><i class="fad fa-check-circle mr-2"></i>Yes</h3>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-primary"><i class="fad fa-globe mr-2"></i>Public</h3>
                    </li>
                    <li  className="flex-10">
                        <p><strong>10$</strong>USD</p>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-success">3</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">10</div>
                    </li>
                    <li  className="flex-15">
                    <p>1/22/2021, 5:30:00 AM</p>
                    </li>

                    <li  className="flex-10">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p>Codes Packag</p>
                        <h2>code catagory</h2>
                    </li>
                    <li className="flex-10">
                        <p>Code</p>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-green"><i class="fad fa-check-circle mr-2"></i>Yes</h3>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-primary"><i class="fad fa-globe mr-2"></i>Public</h3>
                    </li>
                    <li  className="flex-10">
                        <p><strong>10$</strong>USD</p>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-success">3</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">10</div>
                    </li>
                    <li  className="flex-15">
                    <p>1/22/2021, 5:30:00 AM</p>
                    </li>

                    <li  className="flex-10">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p>Codes Packag</p>
                        <h2>code catagory</h2>
                    </li>
                    <li className="flex-10">
                        <p>Code</p>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-green"><i class="fad fa-check-circle mr-2"></i>Yes</h3>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-primary"><i class="fad fa-globe mr-2"></i>Public</h3>
                    </li>
                    <li  className="flex-10">
                        <p><strong>10$</strong>USD</p>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-success">3</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">10</div>
                    </li>
                    <li  className="flex-15">
                    <p>1/22/2021, 5:30:00 AM</p>
                    </li>

                    <li  className="flex-10">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p>Codes Packag</p>
                        <h2>code catagory</h2>
                    </li>
                    <li className="flex-10">
                        <p>Code</p>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-green"><i class="fad fa-check-circle mr-2"></i>Yes</h3>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-primary"><i class="fad fa-globe mr-2"></i>Public</h3>
                    </li>
                    <li  className="flex-10">
                        <p><strong>10$</strong>USD</p>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-success">3</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">10</div>
                    </li>
                    <li  className="flex-15">
                    <p>1/22/2021, 5:30:00 AM</p>
                    </li>
                    <li  className="flex-10">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p>Codes Packag</p>
                        <h2>code catagory</h2>
                    </li>
                    <li className="flex-10">
                        <p>Code</p>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-green"><i class="fad fa-check-circle mr-2"></i>Yes</h3>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-primary"><i class="fad fa-globe mr-2"></i>Public</h3>
                    </li>
                    <li  className="flex-10">
                        <p><strong>10$</strong>USD</p>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-success">3</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">10</div>
                    </li>
                    <li  className="flex-15">
                    <p>1/22/2021, 5:30:00 AM</p>
                    </li>
                    <li  className="flex-10">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p>Codes Packag</p>
                        <h2>code catagory</h2>
                    </li>
                    <li className="flex-10">
                        <p>Code</p>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-green"><i class="fad fa-check-circle mr-2"></i>Yes</h3>
                    </li>
                    <li  className="flex-10">
                        <h3 class="text-primary"><i class="fad fa-globe mr-2"></i>Public</h3>
                    </li>
                    <li  className="flex-10">
                        <p><strong>10$</strong>USD</p>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-success">3</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">10</div>
                    </li>
                    <li  className="flex-15">
                    <p>1/22/2021, 5:30:00 AM</p>
                    </li>
                    <li  className="flex-10">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    </React.Fragment>
  );
}

}

AddOns.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  addOns: makeSelectAddOns(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(AddOns);
