/**
 *
 * Asynchronously loads the component for AddOns
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
