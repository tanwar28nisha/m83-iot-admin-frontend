import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the manageTenants state domain
 */

const selectManageTenantsDomain = state => state.manageTenants || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by ManageTenants
 */

const makeSelectManageTenants = () =>
  createSelector(
    selectManageTenantsDomain,
    substate => substate,
  );

export default makeSelectManageTenants;
export { selectManageTenantsDomain };
