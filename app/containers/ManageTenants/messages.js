/*
 * ManageTenants Messages
 *
 * This contains all the text for the ManageTenants container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ManageTenants';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ManageTenants container!',
  },
});
