/**
 *
 * Asynchronously loads the component for ManageTenants
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
