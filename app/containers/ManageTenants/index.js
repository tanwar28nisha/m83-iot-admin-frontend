/**
 *
 * ManageTenants
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectManageTenants from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export class ManageTenants extends React.Component {
    render() {

  return (
    <React.Fragment>
      <Helmet>
        <title>ManageTenants</title>
        <meta name="description" content="Description of ManageTenants" />
      </Helmet>

    <header className="content-header d-flex">
        <div className="flex-30">
             <h6>Manage Tenant</h6>
        </div>
        <div className="flex-70 position-relative">
            <div className="content-header-group">
                <div className="search-group">
                    <i className="far fa-search"></i>
                    <input type="search" className="form-control" placeholder="Search....." />
                </div>
                <button className="btn btn-primary" onClick = {() => this.props.history.push('/AddOrEditManageTenants')}><i className="far fa-plus"></i>Add New</button>
            </div>
        </div>
    </header>

    <div className="content-body">
        <div className="list-view-wrapper">
            <div className="list-view-header">
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <h1>Tenant Name</h1>
                    </li>
                    <li className="flex-10">
                        <h1>Company</h1>
                    </li>
                    <li  className="flex-25">
                        <h1>Url</h1>
                    </li>
                    <li  className="flex-10">
                        <h1>Type</h1>
                    </li>
                    <li  className="flex-10">
                        <h1>Users</h1>
                    </li>
                    <li  className="flex-15">
                        <h1>Expiring On</h1>
                    </li>
                    <li  className="flex-15">
                        <h1>Actions</h1>
                    </li>
                </ul>
            </div>
            <div className="list-view-body">
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p className="text-blue">Developer</p>
                    </li>
                    <li className="flex-10">
                        <p>Iot83</p>
                    </li>
                    <li  className="flex-25">
                        <a className="text-truncate">https://content.iot83.com/m83/misc/addTenant.png</a>
                    </li>
                    <li  className="flex-10">
                        <div className="text-success alert alert-success">Flex</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">1</div>
                    </li>
                    <li  className="flex-15">
                        <p>1/22/2021, 5:30:00 AM</p>
                    </li>
                    <li  className="flex-15">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-success"><i className="far fa-info"></i></button>
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p className="text-blue">Developer</p>
                    </li>
                    <li className="flex-10">
                        <p>Iot83</p>
                    </li>
                    <li  className="flex-25">
                        <a className="text-truncate">https://content.iot83.com/m83/misc/addTenant.png</a>
                    </li>
                    <li  className="flex-10">
                        <div className="text-success alert alert-success">Flex</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">1</div>
                    </li>
                    <li  className="flex-15">
                        <p>1/22/2021, 5:30:00 AM</p>
                    </li>
                    <li  className="flex-15">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-success"><i className="far fa-info"></i></button>
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p className="text-blue">Developer</p>
                    </li>
                    <li className="flex-10">
                        <p>Iot83</p>
                    </li>
                    <li  className="flex-25">
                        <a className="text-truncate">https://content.iot83.com/m83/misc/addTenant.png</a>
                    </li>
                    <li  className="flex-10">
                        <div className="text-success alert alert-success">Flex</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">1</div>
                    </li>
                    <li  className="flex-15">
                        <p>1/22/2021, 5:30:00 AM</p>
                    </li>
                    <li  className="flex-15">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-success"><i className="far fa-info"></i></button>
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p className="text-blue">Developer</p>
                    </li>
                    <li className="flex-10">
                        <p>Iot83</p>
                    </li>
                    <li  className="flex-25">
                        <a className="text-truncate">https://content.iot83.com/m83/misc/addTenant.png</a>
                    </li>
                    <li  className="flex-10">
                        <div className="text-success alert alert-success">Flex</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">1</div>
                    </li>
                    <li  className="flex-15">
                        <p>1/22/2021, 5:30:00 AM</p>
                    </li>
                    <li  className="flex-15">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-success"><i className="far fa-info"></i></button>
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p className="text-blue">Developer</p>
                    </li>
                    <li className="flex-10">
                        <p>Iot83</p>
                    </li>
                    <li  className="flex-25">
                        <a className="text-truncate">https://content.iot83.com/m83/misc/addTenant.png</a>
                    </li>
                    <li  className="flex-10">
                        <div className="text-success alert alert-success">Flex</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">1</div>
                    </li>
                    <li  className="flex-15">
                        <p>1/22/2021, 5:30:00 AM</p>
                    </li>
                    <li  className="flex-15">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-success"><i className="far fa-info"></i></button>
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-15">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p className="text-blue">Developer</p>
                    </li>
                    <li className="flex-10">
                        <p>Iot83</p>
                    </li>
                    <li  className="flex-25">
                        <a className="text-truncate">https://content.iot83.com/m83/misc/addTenant.png</a>
                    </li>
                    <li  className="flex-10">
                        <div className="text-success alert alert-success">Flex</div>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-primary">1</div>
                    </li>
                    <li  className="flex-15">
                        <p>1/22/2021, 5:30:00 AM</p>
                    </li>
                    <li  className="flex-15">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-success"><i className="far fa-info"></i></button>
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    </React.Fragment>
  );
}
}

ManageTenants.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  manageTenants: makeSelectManageTenants(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(ManageTenants);
