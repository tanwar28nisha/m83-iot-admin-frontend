/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

export default function HomePage() {
  return (
    <React.Fragment>
      <header className="content-header">
      <h6>Home</h6>
    </header>

      <div className="content-body">
        <ul className="counter-list">
            <li>
              <div className="couter-wrapper">
                <div className="counter-logo">
                  <i className="fal fa-building"></i>
                </div>
                <h5>Tenants</h5>
                <p>8</p>
              </div>
            </li>
            <li>
              <div className="couter-wrapper">
                <div className="counter-logo">
                  <i className="fal fa-chart-network"></i>
                </div>
                <h5>Connectors</h5>
                <p>8</p>
              </div>
            </li>
            <li>
              <div className="couter-wrapper">
                <div className="counter-logo">
                  <i className="fal fa-exchange"></i>
                </div>
                <h5>Transformations</h5>
                <p>8</p>
              </div>
            </li>
            <li>
              <div className="couter-wrapper">
                <div className="counter-logo">
                  <i className="fal fa-lambda"></i>
                </div>
                <h5>Code Engines</h5>
                <p>8</p>
              </div>
            </li>
            <li>
              <div className="couter-wrapper">
                <div className="counter-logo">
                  <i className="fal fa-function"></i>
                </div>
                <h5>Functions</h5>
                <p>8</p>
              </div>
            </li>
            <li>
              <div className="couter-wrapper">
                <div className="counter-logo">
                  <i className="fal fa-network-wired"></i>
                </div>
                <h5>APIs</h5>
                <p>8</p>
              </div>
            </li>
            <li>
              <div className="couter-wrapper">
                <div className="counter-logo">
                  <i className="fal fa-window"></i>
                </div>
                <h5>Pages</h5>
                <p>8</p>
              </div>
            </li>
            <li>
              <div className="couter-wrapper">
                <div className="counter-logo">
                  <i className="fal fa-desktop-alt"></i>
                </div>
                <h5>Applications</h5>
                <p>8</p>
              </div>
            </li>
        </ul>
        <div className="d-flex">
          <div className="flex-33 pd-r-7">
            <div className="card">
              <div className="card-body">
                  <div className="circular-chart">
                    <img src="https://i1.wp.com/angularscript.com/wp-content/uploads/2016/01/Angular-Percent-Circle-Directive.png?fit=462%2C392&ssl=1" />
                  </div>
              </div>
            </div>   
          </div>
          <div className="flex-33 pd-r-7 pd-l-7"> 
              <div className="card">
                <div className="card-body">
                    <div className="circular-chart">
                      <img src="https://i1.wp.com/angularscript.com/wp-content/uploads/2016/01/Angular-Percent-Circle-Directive.png?fit=462%2C392&ssl=1" />
                    </div>
                </div>
              </div>
          </div>
          <div className="flex-33 pd-l-7"> 
            <div className="card">
              <div className="card-body">
                  <div className="circular-chart">
                    <img src="https://i1.wp.com/angularscript.com/wp-content/uploads/2016/01/Angular-Percent-Circle-Directive.png?fit=462%2C392&ssl=1" />
                  </div>
              </div>
            </div>  
          </div>
        </div>
        <div className="d-flex">
          <div className="flex-50 pd-r-7"> 
              <div className="card">
                  <div className="card-header">
                    <h1>Activities</h1>
                    <div className="form-group">
                        <select className="form-control">
                          <option>Select</option>
                        </select>
                    </div>
                  </div>
                  <div className="card-body">
                      <div className="chart-box">
                        <p>No chart found !</p>
                      </div>
                  </div>
              </div>
          </div>
          <div className="flex-50 pd-l-7"> 
              <div className="card">
                  <div className="card-header">
                    <h1>Top Tenant Activities</h1>
                    <div className="form-group">
                        <select className="form-control">
                          <option>Select</option>
                        </select>
                    </div>
                  </div>
                  <div className="card-body">
                      <div className="chart-box">
                        <p>No chart found !</p>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
