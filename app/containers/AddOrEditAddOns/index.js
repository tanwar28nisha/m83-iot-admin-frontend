/**
 *
 * AddOrEditAddOns
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectAddOrEditAddOns from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export function AddOrEditAddOns() {
  useInjectReducer({ key: 'addOrEditAddOns', reducer });
  useInjectSaga({ key: 'addOrEditAddOns', saga });

  return (
    <React.Fragment>
      <Helmet>
        <title>AddOrEditAddOns</title>
        <meta name="description" content="Description of AddOrEditAddOns" />
      </Helmet>

    <header className="content-header d-flex">
        <div className="flex-30">
             <h6>Add-Ons</h6>
        </div>
        <div className="flex-70 text-right">
            
        </div>
    </header>
    <div className="content-body" id="addOrEditLicense">
        <div className="form-content-wrapper">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="pill" href="#home">Products Detail</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#menu1">Package Products</a>
                </li>
            </ul>
            
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <div className="form-header">
                        <h6>Products Details</h6>
                    </div>
                    <div className="form-body">
                        <div className="d-flex">
                            <div className="flex-80 pd-r-7">
                            <div className="d-flex">
                            <div className="flex-50 pd-r-7">
                                <div className="form-group">
                                    <label className="form-group-label">Name :</label>
                                    <input type="text" className="form-control" />
                                </div>
                            </div>
                            <div className="flex-50 pd-l-7">
                                <div className="form-group">
                                    <label className="form-group-label">Category : </label>
                                    <input type="text" className="form-control" />
                                </div>
                            </div>
                            <div className="flex-50 pd-r-7">
                                <div className="form-group">
                                    <label className="form-group-label">Description :</label>
                                    <textarea rows="1" type="text" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label class="check-box check-box-group">
                                        <span class="check-box-text">Publish : </span>
                                        <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>   
                                </div>
                            </div>
                            <div className="flex-50 pd-l-7">
                                <div className="form-group">
                                    <label className="form-group-label">Amount(in USD) : </label>
                                    <input type="number" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label class="check-box check-box-group">
                                        <span class="check-box-text">is Private Package ?</span>
                                        <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>   
                                </div>
                            </div>
                        </div>
                            </div>
                            <div className="flex-20 pd-l-7">
                                <div className="form-group">
                                    <label className="form-group-label">Image : </label>
                                    <div className="input-file-upload">
                                        <input type="file" className="form-control" />
                                        <p>Click to Upload File</p>
                                        {/* <div className="file-upload-preview">
                                            <img src="" />
                                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                                            <input type="file" className="form-control" />
                                        </div> */}
                                    </div>
                                </div>
                            </div>
                            <div className="flex-33">
                                <div className="form-group">
                                    <label className="form-group-label">Tenants :</label>
                                    <select className="form-control">
                                        <option>Select</option>
                                    </select>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="menu1">
                    <div className="form-header">
                        <h6>Package Attributes</h6>
                    </div>
                    <div className="form-body">
                        <ul className="package-list">
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="package-list-item">
                                    <label class="check-box">
                                        <span class="check-box-text">FaaS</span>
                                            <input type="checkbox" className="form-control"  />
                                        <span class="check-mark"></span>
                                    </label>
                                    <p>Base Package : <strong>1</strong></p>
                                    <div className="input-group">
                                        <button className="btn btn-light input-group-prepend"><i class="fas fa-chevron-left"></i></button>
                                        <input className="form-control" type="number"/>
                                        <button className="btn btn-light input-group-append"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                </div>
            <div className="form-footer">
                <button className="btn btn-secondary">Cancel</button>
                <button className="btn btn-primary">Save</button>
            </div>
        </div>   
    </div>
    </React.Fragment>
  );
}

AddOrEditAddOns.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  addOrEditAddOns: makeSelectAddOrEditAddOns(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(AddOrEditAddOns);
