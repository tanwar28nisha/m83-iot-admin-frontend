/*
 * AddOrEditAddOns Messages
 *
 * This contains all the text for the AddOrEditAddOns container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.AddOrEditAddOns';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the AddOrEditAddOns container!',
  },
});
