/**
 *
 * Asynchronously loads the component for AddOrEditAddOns
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
