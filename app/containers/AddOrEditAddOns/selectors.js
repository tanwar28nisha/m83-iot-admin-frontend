import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the addOrEditAddOns state domain
 */

const selectAddOrEditAddOnsDomain = state =>
  state.addOrEditAddOns || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by AddOrEditAddOns
 */

const makeSelectAddOrEditAddOns = () =>
  createSelector(
    selectAddOrEditAddOnsDomain,
    substate => substate,
  );

export default makeSelectAddOrEditAddOns;
export { selectAddOrEditAddOnsDomain };
