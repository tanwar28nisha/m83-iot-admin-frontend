/*
 * ManageLicense Messages
 *
 * This contains all the text for the ManageLicense container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ManageLicense';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ManageLicense container!',
  },
});
