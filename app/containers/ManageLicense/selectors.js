import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the manageLicense state domain
 */

const selectManageLicenseDomain = state => state.manageLicense || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by ManageLicense
 */

const makeSelectManageLicense = () =>
  createSelector(
    selectManageLicenseDomain,
    substate => substate,
  );

export default makeSelectManageLicense;
export { selectManageLicenseDomain };
