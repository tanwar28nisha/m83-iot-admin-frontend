/**
 *
 * ManageLicense
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectManageLicense from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export class ManageLicense extends React.Component {
    render() {

  return (
    <React.Fragment>
      <Helmet>
        <title>ManageLicense</title>
        <meta name="description" content="Description of ManageLicense" />
      </Helmet>

    <header className="content-header d-flex">
        <div className="flex-30">
             <h6>Manage Licenses</h6>
        </div>
        <div className="flex-70 position-relative">
            <div className="content-header-group">
                <div className="search-group">
                    <i className="far fa-search"></i>
                    <input type="search" className="form-control" placeholder="Search....." />
                </div>
                <button onClick = {() => this.props.history.push('/AddOrEditManageLicense')} className="btn btn-primary"><i className="far fa-plus"></i>Add New</button>
            </div>
        </div>
    </header>
    <div className="content-body">
        <div className="list-view-wrapper">
            <div className="list-view-header">
                <ul className="list-view d-flex">
                    <li class="flex-20">
                        <h1>Name</h1>
                    </li>
                    <li className="flex-45">
                        <h1>Description</h1>
                    </li>
                    <li  className="flex-10">
                        <h1>Category</h1>
                    </li>
                    <li  className="flex-10">
                        <h1>Validity</h1>
                    </li>
                    <li  className="flex-15">
                        <h1>Actions</h1>
                    </li>
                </ul>
            </div>
            <div className="list-view-body">
                <ul className="list-view d-flex">
                    <li class="flex-20">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p className="text-blue">Maker</p>
                    </li>
                    <li className="flex-45 pd-r-7">
                        <p className="text-truncate">Find image stock images in HD and millions of other royalty-free stock photos, illustrations and vectors in the Shutterstock collection. Thousands of new ...</p>
                    </li>
                    <li  className="flex-10">
                        <p className="text-success">Maker</p>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-success">30</div>
                    </li>
                    <li  className="flex-15">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-success"><i className="far fa-info"></i></button>
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-20">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p className="text-blue">Maker</p>
                    </li>
                    <li className="flex-45 pd-r-7">
                        <p className="text-truncate">Find image stock images in HD and millions of other royalty-free stock photos, illustrations and vectors in the Shutterstock collection. Thousands of new ...</p>
                    </li>
                    <li  className="flex-10">
                        <p className="text-success">Maker</p>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-success">30</div>
                    </li>
                    <li  className="flex-15">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-success"><i className="far fa-info"></i></button>
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-20">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p className="text-blue">Maker</p>
                    </li>
                    <li className="flex-45 pd-r-7">
                        <p className="text-truncate">Find image stock images in HD and millions of other royalty-free stock photos, illustrations and vectors in the Shutterstock collection. Thousands of new ...</p>
                    </li>
                    <li  className="flex-10">
                        <p className="text-success">Maker</p>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-success">30</div>
                    </li>
                    <li  className="flex-15">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-success"><i className="far fa-info"></i></button>
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-20">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p className="text-blue">Maker</p>
                    </li>
                    <li className="flex-45 pd-r-7">
                        <p className="text-truncate">Find image stock images in HD and millions of other royalty-free stock photos, illustrations and vectors in the Shutterstock collection. Thousands of new ...</p>
                    </li>
                    <li  className="flex-10">
                        <p className="text-success">Maker</p>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-success">30</div>
                    </li>
                    <li  className="flex-15">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-success"><i className="far fa-info"></i></button>
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>
                <ul className="list-view d-flex">
                    <li class="flex-20">
                        <div className="list-image">
                            <img src="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg" />
                        </div>
                        <p className="text-blue">Maker</p>
                    </li>
                    <li className="flex-45 pd-r-7">
                        <p className="text-truncate">Find image stock images in HD and millions of other royalty-free stock photos, illustrations and vectors in the Shutterstock collection. Thousands of new ...</p>
                    </li>
                    <li  className="flex-10">
                        <p className="text-success">Maker</p>
                    </li>
                    <li  className="flex-10">
                        <div className="alert alert-success">30</div>
                    </li>
                    <li  className="flex-15">
                        <div className="button-group">
                            <button className="btn btn-transparent btn-transparent-success"><i className="far fa-info"></i></button>
                            <button className="btn btn-transparent btn-transparent-primary"><i className="far fa-pen"></i></button>
                            <button className="btn btn-transparent btn-transparent-danger"><i className="far fa-trash-alt"></i></button>
                        </div>
                    </li>
                </ul>   
            </div>
        </div>
    </div>
    </React.Fragment>
  );
}
}

ManageLicense.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  manageLicense: makeSelectManageLicense(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(ManageLicense);
