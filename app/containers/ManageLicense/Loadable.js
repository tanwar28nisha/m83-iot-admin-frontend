/**
 *
 * Asynchronously loads the component for ManageLicense
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
